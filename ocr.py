import cv2
import numpy as np
import pytesseract

img = cv2.imread('cropped2.jpg')

cv2.imshow('window', img)
cv2.waitKey()


img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

cv2.imshow('window', img)
cv2.waitKey()


img = cv2.medianBlur(img, 5)

cv2.imshow('window', img)
cv2.waitKey()


img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

cv2.imshow('window', img)
cv2.waitKey()


img = cv2.bitwise_not(img)

cv2.imshow('window', img)
cv2.waitKey()

text = pytesseract.image_to_string(img, lang = 'eng')
print(text)